---
tags:
  - Content Health
  - KCS Article State
  - Performance Assessment
  - KCS Roles and the Licensing Model
  - Scenario 07
  - 'Technique 5.12: Self-Service Measures'
  - ❓
---

Status: Unknown ❓

##Mandatory##

The system shall provide visibility management options that uses the user role, from user profiles, along with article quality, article confidence, article governance or other article attributes to determine visibility.  This visibility must at minimum be article level and ideally in-line or at the field or statement level.

##Practice/Technique##

* Content Health
* KCS Article State
* Performance Assessment
* KCS Roles and the Licensing Model

##Reference: KCS v6 Practices Guide##
Technique 5.12: Self-Service Measures

##Demo Requirements for Verified Only##
[Scenario 07](../scenario/07.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
