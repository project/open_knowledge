---
tags:
  - Performance Assessment
  - Assessing the Creation of Value
  - Scenario 09
  - 'Technique 7.3: Assessing the Creation of Value'
  - ❌
---

Status: Incomplete ❌

##Mandatory##

The system shall provide reports showing citation rates for an individual or team (group of users) and knowledge domain (category, tag, or node).

##Practice/Technique##

* Performance Assessment
* Assessing the Creation of Value

##Reference: KCS v6 Practices Guide##
Technique 7.3: Assessing the Creation of Value

##Demo Requirements for Verified Only##
[Scenario 09](../scenario/09.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
