---
tags:
  - Performance Assessment
  - KCS Roles and the Licensing Model
  - Scenario 07
  - 'Technique 7.1: KCS Roles and the Licensing Model'
  - ❓
---

Status: Unknown ❓

##Mandatory##

KCS articles in each level of audience shall be searchable and visible to an appropriate KCS license level. For example, a KCS Contributor might have access to view all articles, a KCS Candidate might have access to view Non-Verified and Verified articles, and customer end-users might have access to only Verified/External articles.

##Practice/Technique##

* Performance Assessment
* KCS Roles and the Licensing Model

##Reference: KCS v6 Practices Guide##
Technique 7.1: KCS Roles and the Licensing Model

##Demo Requirements for Verified Only##
[Scenario 07](../scenario/07.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
