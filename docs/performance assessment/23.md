---
tags:
  - ❓
---

Status: Unknown ❓

##Mandatory##

Mechanism for grouping article activities, individual and group, distinguish between all contributors define in documentation column how individual contribution is extracted, initial author, last modified, assigned owner, all historical contributors across versions.

##Practice/Technique##

##Reference: KCS v6 Practices Guide##


##Demo Requirements for Verified Only##


##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
