---
tags:
  - ❓
---

Status: Unknown ❓

##Mandatory##

Mechanism for integration of reporting tools. In documentation column list all possible export formats.

##Practice/Technique##


##Reference: KCS v6 Practices Guide##


##Demo Requirements for Verified Only##


##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
