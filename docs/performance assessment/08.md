---
tags:
  - Closing Thoughts on the Solve Loop
  - Collective Ownership in the Solve Loop
  - Scenario 09
  - ❓
---

Status: Unknown ❓

##Mandatory##

The system shall provide reports showing history of the KCS article and the individuals that have contributed over a particular period of time. These reports will be capable of segmentation by individual, team (group of users), date, and knowledge domain (category, tag, or role).

##Practice/Technique##

* Closing Thoughts on the Solve Loop
* Collective Ownership in the Solve Loop

##Reference: KCS v6 Practices Guide##
Closing Thoughts on the Solve Loop

##Demo Requirements for Verified Only##
[Scenario 09](../scenario/09.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
