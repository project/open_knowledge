---
tags:
  - Performance Assessment
  - Assessing the Creation of Value
  - Measures for Individuals and Teams
  - Scenario 09
  - 'Technique 7.3: Assessing the Creation of Value'
  - ❓
---

Status: Unknown ❓

##Mandatory##

The system shall provide reports showing documents created, modified, and flagged by anyone who has contributed to any version of the article over time for a particular time period. These reports will be capable of segmentation by individual, team (group of users and knowledge domain (category, tag, or node),

##Practice/Technique##

* Performance Assessment
* Assessing the Creation of Value
* Measures for Individuals and Teams

##Reference: KCS v6 Practices Guide##
Technique 7.3: Assessing the Creation of Value

##Demo Requirements for Verified Only##
[Scenario 09](../scenario/09.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
