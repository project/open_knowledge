---
tags:
  - Reuse
  - Linking
  - Referencing or Linking to Other Information Sources
  - Not Mandatory
  - Scenario 09
  - 'Technique 3.3: Linking'
  - ❎
---

Status: Not Mandatory ❎

##Not Mandatory##

The system shall provide reports showing how often a KCS article was used as a reference over a particular period of time. These reports will be capable of segmentation by individual, team (group of users), knowledge domain (category, tag, or role), and date.

##Practice/Technique##

* Reuse
* Linking
* Referencing or Linking to Other Information Sources

##Reference: KCS v6 Practices Guide##
Technique 3.3: Linking

##Demo Requirements for Verified Only##
[Scenario 09](../scenario/09.md)

##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
