---
tags:
  - Structure
  - Utilize Simple Templates
  - Scenario 01
  - 'Technique 2.1: Use a Simple Template'
  - ✅
  - drupal/core/field
  - drupal/core/field_ui
---

Status: Complete ✅

##Mandatory##

A  KCS article template that includes the following fields:

* Issue
* Environment
* Resolution
* Cause
* Comments and Notes (Feedback from users)

##Practice/Technique##

* Structure
* Utilize Simple Templates

##Reference: KCS v6 Practices Guide##

[Technique 2.1: Use a Simple Template](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/030/020/010)

##Demo Requirements for Verified Only##
[Scenario 01](/scenario/01.md)

##OB/Config/Cust/NS/SF##

###Out of the Box###

**admin/structure/types/manage/article/fields**

The fields are provided by the Core module 'field'. Notes are provided by the Community module, 'Moderation note'.

![Article fields](structure%2001%20fields.png)

##Native Product Capability##

[Field](/tags/#drupalcorefield)

##Mechanism for Config##

A graphic interface is provided by the Core module [Field UI](/tags/#drupalcorefield_ui).

##Config Strategy##

The path and screen shot above shows the administration page for the Article's fields. There are options to Delete, Edit, Create, and Add an Existing field.

##Dependencies & Version Numbers##


##Documentation##
