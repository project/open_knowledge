---
tags:
  - Structure
  - Utilize Simple Templates
  - Scenario 07
  - 'Technique 2.1: Use a Simple Template'
  - ✅
---

Status: Complete ✅

##Mandatory##

At KCS Article capture, user may select which template they use, although a default may be maintained.

##Practice/Technique##

* Structure
* Utilize Simple Templates

##Reference: KCS v6 Practices Guide##

[Technique 2.1: Use a Simple Template](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/030/020/010)

##Demo Requirements for Verified Only##
[Scenario 07](/scenario/07.md)

##OB/Config/Cust/NS/SF##

###Out of the Box###

**node/add**
![Add Node](./structure%2005%20add%20node.png)

##Native Product Capability##

##Mechanism for Config##
A graphic interface is provided for creating new article types.

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
