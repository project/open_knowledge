---
tags:
  - Structure
  - Complete Thoughts, Not Complete Sentences
  - Scenario 01
  - 'Technique 2.2: Complete Thoughts, Not Complete Sentences'
  - ✅
---

Status: Complete ✅

##Mandatory##

Captured KCS Article content from initial entry will support at least basic formatting, including preservation of breaks.  No arbitrary limit on the length of template sections will be imposed.

##Practice/Technique##

* Structure
* Complete Thoughts, Not Complete Sentences

##Reference: KCS v6 Practices Guide##

[Technique 2.2: Complete Thoughts, Not Complete Sentences](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/030/020/020)

##Demo Requirements for Verified Only##
[Scenario 01](../scenario/01.md)

##OB/Config/Cust/NS/SF##
###Out of the Box###
The Core modules 'editor' and 'ckeditor5' provide this ability. Editor allows
different WYSIWYG editors to be used. CKEditor is the default editor.

##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
