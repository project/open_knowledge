---
tags:
  - Structure
  - Utilize Simple Templates
  - Scenario 01
  - 'Technique 2.1: Use a Simple Template'
  - ✅
---

Status: Complete ✅

##Mandatory##

Issue can be captured in the workflow in a template that helps to structure the content.

##Practice/Technique##

* Structure
* Utilize Simple Templates

##Reference: KCS v6 Practices Guide##

[Technique 2.1: Use a Simple Template](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/030/020/010)

##Demo Requirements for Verified Only##
[Scenario 01](../scenario/01.md)

##OB/Config/Cust/NS/SF##
###Out of the Box###
**admin/structure/types/manage/article/fields**

The Issue field is provided out of the box.

##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
