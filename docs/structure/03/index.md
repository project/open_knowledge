---
tags:
  - Structure
  - Utilize Simple Templates
  - Not Mandatory
  - Scenario 01
  - 'Technique 2.1: Use a Simple Template'
  - ✅
  - drupal/core/node
---

Status: Complete ✅

##Not Mandatory##

Template structure of the Article is not alterable by the user when creating or editing an article

##Practice/Technique##

* Structure
* Utilize Simple Templates


##Reference: KCS v6 Practices Guide##

[Technique 2.1: Use a Simple Template](https://library.serviceinnovation.org/KCS/KCS_v6/KCS_v6_Practices_Guide/030/030/020/010)

##Demo Requirements for Verified Only##
[Scenario 01](/scenario/01.md)

##OB/Config/Cust/NS/SF##

###Out of the Box###
**node/add/article**

The template structure is implemented with different fields.

![Node Form](./structure%2003%20node%20form.png)

##Native Product Capability##
[Node](/tags/#drupalcorenode)

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
