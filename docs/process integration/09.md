---
tags:
  - Structure
  - Utilize Simple Templates
  - Not Mandatory
  - 'Technique 5.1: KCS Article Structure'
  - ❓
---

Status: Unknown ❓

##Not Mandatory##

Administrators can adjust KCS Article structured such that the search engine can apply different weights by field, include/exclude fields in the search

##Practice/Technique##

* Structure
* Utilize Simple Templates

##Reference: KCS v6 Practices Guide##
Technique 5.1: KCS Article Structure

##Demo Requirements for Verified Only##


##OB/Config/Cust/NS/SF##


##Native Product Capability##

##Mechanism for Config##

##Config Strategy##

##Dependencies & Version Numbers##


##Documentation##
