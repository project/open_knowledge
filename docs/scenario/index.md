
[Scenario 1: External Self-Service to Incident - New Article](01.md)

[Scenario 2: External Self-Service - Reuse Article. Add and Process Feedback](02.md)

[Scenario 3: Internal Reuse with Incident](03.md)

[Scenario 4: Internal Fix-it with Incident](04.md)

[Scenario 5: KCS Candidate Creates Article with Incident, Check for Duplicates](05.md)

[Scenario 6: Report and Merge Duplicates](06.md)

[Scenario 7: Administration](07.md)

[Scenario 8: Reports](08.md)

[Scenario 9: Unique ID System of Record to Assisted - New Article](09.md)

[Scenario 10: External Self-Service No ID System of Record Explicit Reuse Article](10.md)

[Scenario 11: External Self-Service, No ID System of Record Search Behaviors, Log of Search Strings and Click Streams on Articles](11.md)